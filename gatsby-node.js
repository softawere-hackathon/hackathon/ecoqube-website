/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

const path = require(`path`)
// Log out information after a build is done
exports.onPostBuild = ({ reporter }) => {
  reporter.info(`Your Gatsby site has been built!`)
}
// Create blog pages dynamically
exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const newsArticleTemplate = path.resolve(`./src/templates/news-article.jsx`)
  const result = await graphql(`
    query {
      allPrismicNews {
        edges {
          node {
            uid
          }
        }
      }
    }
  `)

  result.data.allPrismicNews.edges.forEach(edge => {
    createPage({
      path: `/news/${edge.node.uid}`,
      component: newsArticleTemplate,
      context: {
        uid: edge.node.uid,
      },
    })
  })
}