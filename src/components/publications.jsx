import * as React from "react"
import PropTypes from "prop-types"
import Publication from "./publication"

SectionPublications = ({ children }) => (
  <div className="max-w-screen-xl mx-auto px-4 mt-12">
    <div className="border-t border-b border-layout-gray-900 py-12 mt-12">
      <h2 className="text-4xl font-bold font-display">Official publications</h2>
      <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-8 lg:gap-12 mt-8">
        <Publication />
        <Publication />
      </div>
    </div>
  </div>
)

SectionPublications.propTypes = {
  children: PropTypes.node.isRequired,
}

SectionPublications.defaultProps = {
  
}

SectionPublications
