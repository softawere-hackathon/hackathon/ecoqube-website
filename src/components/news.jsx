import * as React from "react"
import PropTypes from "prop-types"
import Teaser from "../components/teaser"
import IconPaper from "../../static/icons/paper.svg";

SectionNews = ({ children }) => (
  <div className="max-w-screen-xl mx-auto px-4 relative z-20 mt-20">
    <div className="flex justify-center items-center">
      <div className="relative inline-block mr-6">
        <img src={'cube-skelleton.svg'} />
        <IconPaper className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 -mt-1" />
      </div>
      <h2 className="text-4xl lg:text-5xl font-bold font-display text-center">We have some news</h2>
    </div>
    <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-8 lg:gap-12 mt-12">
      <Teaser />
      <Teaser />
      <Teaser />
    </div>
    <div className="mt-12 md:flex md:justify-center md:items-center text-center md:text-left">
      <p className="text-3xl md:mr-6 mb-4 md:mb-0">Do you wanna have some more news?</p>
      <Button>See all News</Button>
    </div>
  </div>
)

SectionNews.propTypes = {
  children: PropTypes.node.isRequired,
}

SectionNews.defaultProps = {
  
}

SectionNews
