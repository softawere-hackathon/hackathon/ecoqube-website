import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import Button from "../components/button"

const AttentionBanner = ({ children }) => (
  <div className="bg-secondary-base px-4 py-2 flex items-center justify-center w-full">
    <p className="mr-4"><span className="font-bold">Update:</span> We have uploaded a new press release.</p>
    <Button>Learn more</Button>
  </div>
)

AttentionBanner.propTypes = {
  children: PropTypes.node.isRequired,
}

AttentionBanner.defaultProps = {
  
}

export default AttentionBanner
