import * as React from "react"
import PropTypes from "prop-types"
import { StaticImage } from "gatsby-plugin-image"
import IconTurkey from "../../static/icons/flag-turkey.svg";
/*import IconGermany from "../../static/icons/flag-germany.svg";
import IconSweden from "../../static/icons/flag-sweden.svg";
import IconSpain from "../../static/icons/flag-spain.svg";
import IconNetherlands from "../../static/icons/flag-netherlands.svg";
import IconEurope from "../../static/icons/flag-europe.svg";
import IconSwitzerland from "../../static/icons/flag-switzerland.svg";*/

const Card = ({ title, text, country, icon }) => (
  <div className="rounded bg-white p-6 text-layout-gray-base">
    <img src={'lande.png'} className="w-full" />
    <p className="font-bold text-2xl mt-4">{title}</p>
    <div className="inline-flex items-center mt-1">
      <IconTurkey />
      <span className="ml-2 text-layout-gray-700">{country}</span>
    </div>
    <p className="mt-4">{text}</p>
  </div>
)

Card.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  country: PropTypes.string,
}

Card.defaultProps = {
}

export default Card
