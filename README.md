<p align="center">
  <a href="https://www.gatsbyjs.com">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Ecoqube Website
</h1>

Based on Gatsby, Tailwind and Prismic as CMS

## 🚀 Quick start

1.  **Set Up Credentials.**

    Add an .env File to the root and insert the following variables, which can be found in prismic (Settings > API & Security)

    ```shell
    GATSBY_PRISMIC_REPO_NAME=YYYYY
    PRISMIC_ACCESS_TOKEN=YYYYY
    PRISMIC_CUSTOM_TYPES_API_TOKEN=YYYYY
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    npm install
    gatsby develop
    ```
    
3. **Deployment**

    Currently we deploy using AWS Amplify. A push to `main` branch generates a deploy.
    
    *[Here](https://www.gatsbyjs.com/docs/how-to/previews-deploys-hosting/deploying-to-aws-amplify/)'s a description on how to set it up.
    I also had to remove some default redirect which was causing all paths to be redirected to the main homepage, see [here](https://dev.to/narenandu/gatsby-site-hosted-on-aws-amplify-redirecting-to-homepage-always-foo).*
