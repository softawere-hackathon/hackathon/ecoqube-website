module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      display: ['Noto Sans SC', "sans-serif"],
      body: ['-apple-system', 'system-ui', 'BlinkMacSystemFont', "Segoe UI", 'Roboto', 'Helvetica Neue', 'Arial', 'sans-serif'],
    },
    colors: {
      white: "#ffffff",
      black: "#000000",
      transparent: "transparent",
      primary: {
        base: "#68C0AC",
        900: "#77C6B4",
        800: "#86CDBD",
        700: "#95D3C5",
        600: "#A4D9CD",
        500: "#B3DFD6",
        400: "#C3E6DE",
        300: "#D2ECE6",
        200: "#E1F2EE",
        100: "#F0F9F7",
        50: "#F7FCFB"
      },
      secondary: {
        base: "#00A5D3",
        900: "#19AED7",
        800: "#33B7DC",
        700: "#4CC0E0",
        600: "#66C9E5",
        500: "#80D2E9",
        400: "#99DBED",
        300: "#B2E4F2",
        200: "#CCEDF6",
        100: "#E5F6FB",
        50: "#F2FAFD",
      },
      "layout-darkblue": {
        base: "#005970",
        900: "#196A7E",
        800: "#337A8D",
        700: "#4C8B9B",
        600: "#669BA9",
        500: "#80ACB7",
        400: "#99BDC6",
        300: "#B2CDD4",
        200: "#CCDEE2",
        100: "#E5EEF1",
        50: "#F2F7F8",
      },
      "layout-gray": {
        base: "#12171C",
        900: "#2A2E33",
        800: "#414549",
        700: "#595D60",
        600: "#717477",
        500: "#888B8D",
        400: "#A0A2A4",
        300: "#B8B9BB",
        200: "#D0D1D2",
        100: "#E7E8E8",
        50: "#F3F3F4",
      },
      "layout-ocean": {
        base: "#067A9D"
      },
      "layout-highlight": {
        base: "#D7BE3F"
      },
      danger: {
        base: "#DA5656"
      },
    },
    extend: {
      scale: {
        '99': '.99'
      },
      borderRadius: {
        DEFAULT: '12px',
        'lg': '.35rem'
      },
      maxHeight: {
        'modal': '80vh'
      },
      width: {
        'modal': '900px'
      }
    },
  },
  variants: {
    extend: {
      display: ['group-hover', 'responsive']
    },
  },
  plugins: [
    'gatsby-plugin-postcss',
    require('@tailwindcss/typography'),
  ],
}
